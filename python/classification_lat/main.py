from catalog import *
import healpy as hp
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors

cmap = matplotlib.colors.ListedColormap(["white", "red", "blue"])

mycatalog = catalog(
    "/home/qg311713/bachelor/python/classification_lat/gll_psc_v27.fit", 1
)
mycatalog()
df = mycatalog.pdTable

##### class1 and class2 replace " " and "unk" with ""
df["CLASS1"] = df["CLASS1"].map(lambda x: x.replace(" ", ""))
df["CLASS2"] = df["CLASS2"].map(lambda x: x.replace(" ", ""))
df["CLASS1"] = df["CLASS1"].map(lambda x: x.replace("unk", ""))
df["CLASS2"] = df["CLASS2"].map(lambda x: x.replace("unk", ""))
unique1 = df["CLASS1"].unique()  # select unique values from the column
unique2 = df["CLASS2"].unique()
print("######################## INITIAL DF ###########################")
print(f"initial df shape = {df.shape}")
print(f"unique1 = {unique1}")
print(f"unique2 = {unique2}")

df_initial = df.copy()

##### reduce to only 1 source_class
df["CLASS"] = df["CLASS1"] + df["CLASS2"]
df = df.drop(["CLASS1", "CLASS2"], axis=1)
unique = df["CLASS"].unique()

print("######################## MERGED DF ###########################")
print(f"merged df shape = {df.shape}")
print(f"unique = {unique}")

df_agn = df[(df["CLASS"] == "AGN") | (df["CLASS"] == "agn")]
df_agn["CLASS"] = df_agn["CLASS"].map(lambda x: x.upper())
print(f"Number of AGNs : {len(df_agn)}")


df_PSR = df[df["CLASS"] == "PSR"]
print(f"Number of PSRs : {len(df_PSR)}")

NSIDE = 32
NPIX = hp.nside2npix(NSIDE)
agn_pix = hp.ang2pix(
    NSIDE, df_agn["RAJ2000"].to_numpy(), df_agn["DEJ2000"].to_numpy(), lonlat=True
)
PSR_pix = hp.ang2pix(
    NSIDE, df_PSR["RAJ2000"].to_numpy(), df_PSR["DEJ2000"].to_numpy(), lonlat=True
)
m = np.zeros(NPIX)
m[agn_pix] = 1
m[PSR_pix] = 2
hp.mollview(m, title="Mollview AGNs and PSRs", cmap=cmap, coord="E")
plt.savefig("AGNs_PSRs.png")

print(df_PSR.head(5))
