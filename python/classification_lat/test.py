import numpy as np
import healpy as hp

NSIDE = 32
print(
    "Approximate resolution at NSIDE {} is {:.2} deg".format(
        NSIDE, hp.nside2resol(NSIDE, arcmin=True) / 60
    )
)
NPIX = hp.nside2npix(NSIDE)
print(NPIX)
vec = hp.ang2vec(np.pi / 2, np.pi * 3 / 4)
print(vec)
ipix_disc = hp.query_disc(nside=32, vec=vec, radius=np.radians(10))
print(ipix_disc)
